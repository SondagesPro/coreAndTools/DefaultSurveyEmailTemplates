<?php

/**
 * Set the default email template
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021 Denis Chenu <http://www.sondages.pro>
 * @copyright 2021 SGAR21 / Secrétariat général pour les affaires régionales - Bourgogne-Franche-Comté
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class DefaultSurveyEmailTemplates extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Replace the default survey email templates text globally.';
    protected static $name = 'DefaultSurveyEmailTemplates';
    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => '<div class="">' .
                 '<p class="well">If you leave empty string : default limesurvey text still used.</p>' . 
                 '<p class="alert alert-danger"><strong>Warning:</strong> Same text is used for all languages.</p>' .
                 '<p class="alert alert-info">No HTML editor, you can copy/paster HTML source if needed. You have to add yourself html tag.</p>' .
                 '</div>',
        ),
        'htmlemail' => array(
            'type' => 'boolean',
            'label' => 'Use html email',
            'default' => 1,
            //'help' => 'If survey html email is different of this : default was not used.',
        ),
        'email_invite_subj' => array(
            'type' => 'string',
            'label' => 'Invitation email subject:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
                'maxlength' => 255
            ),
        ),
        'email_invite' => array(
            'type' => 'text',
            'label' => 'Invitation email body:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
        'email_remind_subj' => array(
            'type' => 'string',
            'label' => 'Invitation email subject:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
                'maxlength' => 255
            ),
        ),
        'email_remind' => array(
            'type' => 'text',
            'label' => 'Invitation email body:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
        'email_register_subj' => array(
            'type' => 'string',
            'label' => 'Invitation email subject:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
                'maxlength' => 255
            ),
        ),
        'email_register' => array(
            'type' => 'text',
            'label' => 'Invitation email body:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
        'email_confirm_subj' => array(
            'type' => 'string',
            'label' => 'Invitation email subject:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
                'maxlength' => 255
            ),
        ),
        'email_confirm' => array(
            'type' => 'text',
            'label' => 'Invitation email body:',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => "Leave default",
            ),
        ),
    );
    /** @inheritdoc **/
    public function init()
    {
        $this->subscribe('beforeSurveyLanguageSettingSave', 'setDefaultEmailTemplate');
        $this->subscribe('beforeSurveySave', 'setDefaultSurveyHtmlemail');
    }

    /**
     * Set the default email text when create new Language Settings
     * Attach to beforeSurveyLanguageSettingSave event
     * @see https://manual.limesurvey.org/BeforeModelSave
     * @return void
     */
    public function setDefaultEmailTemplate()
    {
        $model = $this->getEvent()->get('model');
        if($model->isNewRecord){
            $settings = array(
                'email_invite',
                'email_remind',
                'email_register',
                'email_confirm',
            );
            foreach ($settings as $setting) {
                tracevar([$setting,$this->get($setting)]);
                if (!empty($this->get($setting))) {
                    $model->setAttribute("surveyls_" . $setting, $this->get($setting) );
                }
                $subject = $setting . "_subj";
                if (!empty($this->get($subject))) {
                    $model->setAttribute("surveyls_" . $subject, $this->get($subject) );
                }
            }
        }
    }

    /**
     * Set the default email settings
     * Attach to beforeSurveySave event
     * @see https://manual.limesurvey.org/BeforeModelSave
     * @return void
     */
    public function setDefaultSurveyHtmlemail()
    {
        $model = $this->getEvent()->get('model');
        if($model->isNewRecord){
            if (!$this->get('htmlemail', null, null, 1)) {
                $model->htmlemail = "N";
            }
        }
    }
}
